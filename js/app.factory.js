
app.factory('Item', ['$http', '$q', '$timeout', function($http, $q, $timeout) {
    function Item(data) {
        this.code = data && angular.isDefined(data.code) ? data.code : '';
        this.name = data && angular.isDefined(data.name) ? data.name : '';
        this.type = data && angular.isDefined(data.type) ? data.type : '';
        this.parent = null;
    }

    return Item;
}]);