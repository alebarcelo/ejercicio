
var themeConfig = ['$mdThemingProvider', function($mdThemingProvider) {

	$mdThemingProvider.theme('dark', 'default')
		.primaryPalette('blue')
		.dark();

}];

app.config(themeConfig);
