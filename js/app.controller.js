
var AppCtrl = ['$scope', 'ajaxService', 'ItemService', function($scope, ajaxService, ItemService) {
    var self = this;
    self.ruta = 'api/v1/data.txt';
    self.types = ['departamento','provincia','distrito'];
    self.items = [];
    self.loading = false;


    self.getData = function(){
        self.items = [];
        self.loading = true;
        ajaxService.getData( self.ruta )
            .then(function(response){

                var data = response && angular.isDefined(response.data) && response.data,
                    //Creo array por salto de línea
                    dataArr = data && angular.isString(data) && data.split('\n');

                angular.isArray(dataArr) && angular.forEach(dataArr, function (item) {
                    self.createItem(item);
                });

                self.loading = false;

            }, function(){
                self.loading = false;
            });
    };

    self.createItem = function(item){
        //Creo array separado por tipo de item
        var itemArr = item.split('/');

        if( angular.isArray(itemArr) ){

            //departamento
            var departamentoData = itemArr[0];
            if( angular.isDefined(departamentoData) ){
                var departamento = ItemService.get(departamentoData, self.types[0]);
                departamento && self.items.push(departamento);
            }

            //provincia
            var provinciaData = itemArr[1];
            if( angular.isDefined(provinciaData) ){
                var provincia = ItemService.get(provinciaData, self.types[1]);
                if( provincia ){
                    provincia.parent = departamento;
                    self.items.push(provincia);
                }
            }

            //distrito
            var distritoData = itemArr[2];
            if( angular.isDefined(provinciaData) ){
                var distrito = ItemService.get(distritoData, self.types[2]);
                if( distrito ){
                    distrito.parent = provincia;
                    self.items.push(distrito);
                }
            }

        }

    };

    self.hasData = function(){
        return self.items.length > 0;
    }

}];

app.controller('AppCtrl', AppCtrl);