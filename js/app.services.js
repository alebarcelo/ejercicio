app.service('ajaxService', ['$http', '$q', '$timeout', function($http, $q, $timeout) {

    this.getData = function( ruta ) {
        var deferred = $q.defer();

        if( ruta == 'api/v1/data.txt' ){
            $timeout(function() {
                deferred.resolve( $http.get( ruta ) );
            }, 2000);
        } else {
            $timeout(function() {
                deferred.reject();
            }, 2000);
        }


        return deferred.promise;
    };

}]);

app.service('ItemService', ['Item', function(Item) {

    this.get = function(data, type){
        var item = this.getDataFromItem(data);
        
        if( !item.code || !item.name ) return false;
        
        return new Item({
            code    : item.code,
            name    : item.name,
            type    : type
        });
    };

    this.getDataFromItem = function (data) {
        data = data.replace('"', '');
        data = $.trim(data);

        var code = data.substr(0, data.indexOf(' '));
        var name = data.substr(data.indexOf(' ') + 1);

        return {
            code    : code,
            name    : name
        }
    };

}]);